//
//  ContentView.swift
//  Memorize
//
//  Created by Nguyễn Thị Thu Quyền on 14/09/2021.
//

import SwiftUI

struct ContentView: View {
    var emoijs = ["🐶", "🐱", "🐭","🐹","🐰","🦊","🐻","🐼","🐻‍❄️","🌸","🌼","🌞","🌻","🌺","🥀","🌹","🌷","💐"];
    @State var emojiCount = 7;
    var body: some View {
        VStack{
            ScrollView{
                LazyVGrid(columns: [GridItem(.adaptive(minimum: 100))]){
            ForEach(emoijs[0..<emojiCount], id: \.self) {
                emoij in
                CardView(content: emoij).aspectRatio(2/3, contentMode: /*@START_MENU_TOKEN@*/.fill/*@END_MENU_TOKEN@*/);

            }        }}.foregroundColor(.red);
            Spacer()
        HStack{
            remove
            Spacer()
            add
        }.font(.largeTitle).padding(.horizontal)
        }.padding(.horizontal)

    }
    
    var remove: some View {
        Button(action: {
                if(emojiCount > 1){
                    emojiCount-=1;
                }
        }, label: {
            Image(systemName: "minus.circle")
        })
    }
    
    var add: some View {
        Button(action: {
                if(emojiCount < emoijs.count){
                    emojiCount += 1;
                }
        }, label: {
                Image(systemName: "plus.circle")            })
    }
}

struct CardView: View {
    var content: String;
    @State var isFaceUp: Bool = true;
    
    var body: some View{
        ZStack{
            let shape = RoundedRectangle(cornerRadius: 20);
            if(isFaceUp){
                shape.stroke(lineWidth: 3);
                Text(content)
                    .font(.largeTitle);
            }else{
                shape;
            }
            
        }.onTapGesture {
            isFaceUp = !isFaceUp;
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
