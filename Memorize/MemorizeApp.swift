//
//  MemorizeApp.swift
//  Memorize
//
//  Created by Nguyễn Thị Thu Quyền on 14/09/2021.
//

import SwiftUI

@main
struct MemorizeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
